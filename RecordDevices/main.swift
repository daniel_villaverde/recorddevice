//
//  main.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 03/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import Foundation
import AVFoundation

var controller = DeviceController()
var utils = Utils()

var devicesList = [Device]()
devicesList = controller.getAttachedDevices()

let args = CommandLine.arguments

if args.count <= 1 || args.count > 4 {
        utils.printHelp()

}else{
    switch CommandLine.arguments[1]{
    case "-ls":
        for device in devicesList{
            if let name = device.name, let id = device.id {
                print("name = \(name) id = \(id)" )
            }
        }
    case "-r":
        if args.count < 3 || args.count > 4 {
            print("recording ... in deviceID \(args.count)")

            utils.printHelp()
        }else{
            print("recording ... in deviceID \(args[2])")
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            let nameRecord =  "video"
            
            if let device = devicesList.filter({$0.id == args[2]}).first{
                let pathAndName = args.count == 4 ? "\(args[3]).mov": "\(documentsPath)/\(nameRecord).mov"
                device.startRecordingDevice(pathAndName)
                device.stopRecordingDevice()
            }else{
                print("The device with Id \(args[2]) is not connected")

            }
        }
   
    default:
        utils.printHelp()
    }
}


















