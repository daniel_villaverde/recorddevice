//
//  VideoDelegate.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 05/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import Foundation
import AVFoundation

class VideoDelegate : NSObject, AVCaptureFileOutputRecordingDelegate
{
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        print("finish recording to \(outputFileURL)")
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        print("started recording to \(fileURL)")
    }
    
}
