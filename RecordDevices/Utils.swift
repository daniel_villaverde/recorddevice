//
//  Utils.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 05/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import Foundation

class Utils{
    
    func printHelp(){
        print("---------------------------------------------")
        print("record iphone video: -r (id) [videoName]")
        print("print attached devices: -ls ")
        print("to stop recording write anything")
        print("---------------------------------------------")
    }
}
