//
//  Controller.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 05/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import Foundation
import AVFoundation
import CoreMediaIO

class DeviceController {
    
    func getAttachedDevices()->[Device]{
        
        var deviceList = [Device]()
        getPermissions()
        let devices = AVCaptureDevice.devices()
        if let devices = devices {
            for device in devices {
                let device = device as! AVCaptureDevice
                deviceList.append(Device.init(device: device))
            }
        }

        return deviceList
    }
    
    fileprivate func getPermissions() {
        
        var allow : UInt32 = 1
        let dataSize : UInt32 = 4
        let zero : UInt32 = 0
        
        var prop = CMIOObjectPropertyAddress(
            mSelector: CMIOObjectPropertySelector(kCMIOHardwarePropertyAllowScreenCaptureDevices),
            mScope: CMIOObjectPropertyScope(kCMIOObjectPropertyScopeGlobal),
            mElement: CMIOObjectPropertyElement(kCMIOObjectPropertyElementMaster))
        
        CMIOObjectSetPropertyData(CMIOObjectID(kCMIOObjectSystemObject), &prop, zero, nil, dataSize, &allow)
    }
}

