//
//  RecordMovie.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 05/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import AVFoundation

class RecordMovie: NSObject {
    
    var session: AVCaptureSession?
    var avDevice : AVCaptureDevice?
    var input : AVCaptureDeviceInput?
    var fileOutput : AVCaptureMovieFileOutput?
    var videoDelegate : VideoDelegate?
    
    convenience init(device: Device){
        self.init()
        avDevice = device.avDevice
        session = AVCaptureSession()
        input = AVCaptureDeviceInput()
        fileOutput = AVCaptureMovieFileOutput()
        videoDelegate = VideoDelegate()
    }
    
    func startRecording(_ pathAndName: String){
        
        if let session = session , let fileOutput = fileOutput{
            input = (try! AVCaptureDeviceInput(device: avDevice))
            session.addInput(input)
            session.addOutput(fileOutput)
            
            let outputFileUrl = URL(fileURLWithPath: pathAndName)
            
            session.startRunning()
            fileOutput.startRecording(toOutputFileURL: outputFileUrl, recordingDelegate: videoDelegate)
        }
    }
    
    func endRecording(){
        
        let keyboard = FileHandle.standardInput
        var cont = true
        while (cont){
            let inputData = keyboard.availableData
            let strData = NSString(data: inputData, encoding: String.Encoding.utf8.rawValue)!
            if strData.trimmingCharacters(in: CharacterSet.newlines) != "" {cont = false}
        }

        if let fileOutput = fileOutput, let session = session{
            fileOutput.stopRecording()
            session.stopRunning()
        }

    }


}
