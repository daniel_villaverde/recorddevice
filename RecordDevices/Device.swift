//
//  Device.swift
//  RecordDevices
//
//  Created by Daniel García Villaverde on 03/08/16.
//  Copyright © 2016 Mobgen. All rights reserved.
//

import AVFoundation


class Device: NSObject {
    
    var id : String?
    var name: String?
    var model: String?
    var record : RecordMovie?
    var avDevice : AVCaptureDevice?
    
    convenience init(device: AVCaptureDevice){
        self.init()
        id       = device.uniqueID
        name     = device.localizedName
        model    = device.modelID
        record   = RecordMovie(device: self)
        avDevice = device
    }
    
    func startRecordingDevice (_ pathAndName: String){
        if let record = record{
            record.startRecording(pathAndName)
        }
    }
    
    func stopRecordingDevice (){
        if let record = record{
            record.endRecording()
        }
    }
    
}
